
## Predicción y Feedback de fractura de muñeca

### `/predict-fracture`

Este endpoint está diseñado para recibir:

- **Imágen de una radiografía de muñeca:** En formato `jpg`, `jpeg` o `png`.
  
Además, se requieren los siguientes campos:

- **ID:** Campo de texto correspondiente al identificador del paciente.
- **Dolor con limitación de movimientos:** Respuesta booleana indicando la presencia de dolor con limitación de movimientos en la muñeca.
- **Edema:** Respuesta booleana indicando si hay edema.
- **Deformidad:** Respuesta booleana indicando la presencia de deformidad.
- **Fecha de nacimiento:** Campo de tipo texto para ingresar la fecha de nacimiento del paciente.
- **Sexo:** Campo de texto para especificar el sexo del paciente.
- **Edad:** Campo numérico para ingresar la edad del paciente.
- **Peso:** Campo numérico para ingresar el peso del paciente.

### Ejemplo de Uso:

```http
POST /predict-fracture/
Content-Type: multipart/form-data

{
  "file": imagen en formato jpg/jpeg/png,
  "id": "string",
  "dolor_con_limitación": true,
  "edema": true,
  "deformidad": false,
  "fecha_nacimiento": "YYYY-MM-DD",
  "sexo": "string",
  "peso": 70.5,
  "edad": 25
}
```

### `/feedback-fracture`

Este endpoint se utiliza para proporcionar feedback específico sobre las imágenes fracturas de muñeca. Requiere los siguientes campos:

- **ID de la imagen:** Campo obligatorio que identifica la imagen a la que se refiere el feedback.
- **Fractura de muñeca:** Etiqueta booleana indicando si hay una fractura en la muñeca.
- **Muñeca sana:** Etiqueta booleana indicando si la muñeca está sana.
- **Comentario:** Campo opcional habilitada en el caso de que las anteriores etiquetas no representativas de la lesión en la imagen.

Solo una de las etiquetas (Fractura de muñeca o Muñeca sana) puede ser verdadera a la vez.

### Ejemplo de Uso:

```http
POST /feedback-lca/
Content-Type: application/json

{
  "image_id": "string",
  "rotura_lca": false,
  "lca_sano": false,
  "comentario": "Fisura."
}
```


## Predicción y Feedback de ligamento cruzado anterior

### `/predict-lca`

Este endpoint está diseñado para recibir un archivo zip que debe contener los siguientes elementos:

- **9 imágenes centrales correspondientes a una resonancia del ligamento cruzado anterior:** Las imágenes deben estar en formato `jpg`, `jpeg` o `png`.
  
Además, se requieren los siguientes campos:

- **ID:** Campo de texto correspondiente al identificador del paciente.
- **Inestabilidad:** Respuesta booleana indicando si el paciente padece inestabilidad.
- **Prueba del cajón anterior:** Respuesta booleana con el resultado de la prueba del cajón anterior (positiva o negativa).
- **Impotencia funcional en la rodilla:** Respuesta booleana indicando si hay impotencia funcional debido a la afectación en la rodilla.
- **Fecha de nacimiento:** Campo de tipo texto para ingresar la fecha de nacimiento del paciente.
- **Sexo:** Campo de texto para especificar el sexo del paciente.
- **Edad:** Campo numérico para ingresar la edad del paciente.
- **Peso:** Campo numérico para ingresar el peso del paciente.

### Ejemplo de Uso:

```http
POST /predict-lca/
Content-Type: multipart/form-data

{
  "file": [9 imágenes en formato jpg/jpeg/png],
  "id": "string",
  "inestabilidad": true,
  "prueba_cajon_anterior_positiva": true,
  "impotencia_funcional_rodilla": false,
  "fecha_nacimiento": "YYYY-MM-DD",
  "sexo": "string",
  "peso": 70.5,
  "edad": 25
}
```

### `/feedback-lca`

Este endpoint se utiliza para proporcionar feedback específico sobre las imágenes del ligamento cruzado anterior (LCA). Requiere los siguientes campos:

- **ID de la imagen:** Campo obligatorio que identifica la imagen a la que se refiere el feedback.
- **Rotura LCA:** Etiqueta booleana indicando si hay una rotura del ligamento cruzado anterior (LCA).
- **LCA sano:** Etiqueta booleana indicando si el ligamento cruzado anterior (LCA) está sano.
- **Comentario:** Campo opcional habilitada en el caso de que las anteriores etiquetas no representativas de la lesión en la imagen.

Solo una de las etiquetas (Rotura LCA o LCA sano) puede ser verdadera a la vez.

### Ejemplo de Uso:

```http
POST /feedback-lca/
Content-Type: application/json

{
  "image_id": "string",
  "rotura_lca": false,
  "lca_sano": false,
  "comentario": "Rotura en el ligamento colateral lateral."
}
```


## Predicción y Feedback de electrocardiograma

### `/predict-egc`

Este endpoint está diseñado para recibir:

- **Imágen de una electrocardiograma:** En formato `jpg`, `jpeg` o `png`.
  
Además, se requieren los siguientes campos:

- **ID:** Campo de texto correspondiente al identificador del paciente.
- **Palpitaciones:** Respuesta booleana indicando la presencia de palpitaciones en el corazón.
- **Dolor toracico irradiado a cuello, mandíbula o miembro superior izquierdo:** Respuesta booleana indicando si hay dolor.
- **Disnea:** Respuesta booleana indicando si hay dificultad respiratoria o falta de aire.
- **Fecha de nacimiento:** Campo de tipo texto para ingresar la fecha de nacimiento del paciente.
- **Sexo:** Campo de texto para especificar el sexo del paciente.
- **Edad:** Campo numérico para ingresar la edad del paciente.
- **Peso:** Campo numérico para ingresar el peso del paciente.

### Ejemplo de Uso:

```http
POST /predict-egc/
Content-Type: multipart/form-data

{
  "file": imagen en formato jpg/jpeg/png,
  "id": "string",
  "palpitaciones": true,
  "dolor_superior_izquierdo": true,
  "disnea": false,
  "fecha_nacimiento": "YYYY-MM-DD",
  "sexo": "string",
  "peso": 70.5,
  "edad": 25
}
```

### `/feedback-egc`

Este endpoint se utiliza para proporcionar feedback específico sobre las imágenes de electrocardiograma. Requiere los siguientes campos:

- **ID de la imagen:** Campo obligatorio que identifica la imagen a la que se refiere el feedback.
- **Fusion de latido ventricular y normal:** Etiqueta booleana indicando la existencia o no de esta afección.
- **Infarto de miocardio:** Etiqueta booleana indicando si la persona está sufriendo o sufrió un infarto.
- **Latido normal:** Etiqueta booleana indicando si el corazón está sano.
- **Latido no clasificable:** Etiqueta booleana que indica la presencia de signos no clasificables en el marco de estas etiquetas.
- **Latido prematuro supraventricular:** Etiqueta booleana indicando la existencia o no de esta afección.
- **Contraccion ventricular prematura:** Etiqueta booleana indicando la existencia o no de esta afección.
- **Comentario:** Campo opcional habilitada en el caso de que las anteriores etiquetas no representativas de la lesión en la imagen.

Solo una de las etiquetas (Fractura de muñeca o Muñeca sana) puede ser verdadera a la vez.

### Ejemplo de Uso:

```http
POST /feedback-egc/
Content-Type: application/json

{
  "image_id": "string",
  "fusion": false,
  "infarto": false,
  "latido_normal": false,
  "no_clasificable": false,
  "prematuro_supraventricular": false,
  "contraccion": false;
  "comentario": "Soplo."
}
```

## Descarga de Bases de Datos

Estos endpoints permiten descargar bases de datos específicas correspondientes a distintos modelos. Cada uno de ellos requiere un código de acceso como parámetro, que permite habilitar la descarga de la base de datos respectiva.

### `/download-bd-lca`

Descarga la base de datos asociada al modelo de ligamento cruzado anterior (LCA).

- **Parámetro:**
  - **Código de Acceso (ID):** Número entero que habilita la descarga de la base de datos del LCA.

### `/download-bd-fracture`

Permite la descarga de la base de datos relacionada con el modelo de fractura de muñeca.

- **Parámetro:**
  - **Código de Acceso (ID):** Número entero necesario para descargar la base de datos de fracturas.

### `/download-bd-egc`

Este endpoint facilita la descarga de la base de datos del modelo de electrocardiograma (EGC).

- **Parámetro:**
  - **Código de Acceso (ID):** Número entero requerido para descargar la base de datos del EGC.

### Ejemplo de Uso:

```http
GET /download-bd-lca?id=123
GET /download-bd-fracture?id=456
GET /download-bd-egc?id=789
```