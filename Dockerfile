# Utiliza la imagen base de Python para Flask
FROM python:3.9

# Establece el directorio de trabajo en /app
WORKDIR /app

# Actualiza el sistema y luego instala libGL
RUN apt-get update && apt-get install -y libgl1-mesa-glx

# Copia el contenido de tu aplicación Flask al contenedor
COPY . /app

# Instala las dependencias de tu aplicación Flask
RUN pip install -r requirements.txt

# Define la variable de entorno para el puerto 8080
ENV PORT 8080

# Expone el puerto 8080
EXPOSE 8080

# Inicia tu aplicación Flask
CMD ["gunicorn", "-b", "0.0.0.0:8080", "app:app"]
