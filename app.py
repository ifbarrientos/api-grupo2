import base64
import os
import tempfile
from io import BytesIO
from zipfile import ZipFile
from flask import Flask, jsonify, request, redirect, url_for, send_file
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from utils import *
from Heart_Pred import predict_egc
from Knee_Pred import predict_lca
from Wrist_Pred import predict_fracture
import hashlib
from static import Valid_hash
from Upload_GS import *

app = Flask(__name__)

CORS(app)


SWAGGER_URL = "/swagger-ui"
API_URL = "/static/swagger.json"
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL, API_URL, config={"app_name": "DiagnosticarIA"}
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)
app.debug = True


########### Datos Generales ############
csv_headers = [
    "ID_Imagen",
    "Imagen",
    "Fecha_Nac",
    "Peso",
    "Altura",
    "Sexo",
    "Dolor",
    "Palpitaciones",
    "Disnea",
    "Respuesta_Medico",
    "Comentario",
]


@app.route("/")
def redirect_to_swagger():
    return redirect(url_for("swagger_ui.show"))


################## Rodilla ##################


@app.route("/predict-lca", methods=["POST"])
def predict_lca_endpoint():
    # Verificar que se haya proporcionado un archivo Zip válido
    if "file" not in request.files:
        return jsonify(
            {"error": "No se proporcionó una imagen válida en el formulario."}, 400
        )

    file = request.files["file"]

    if file.filename == "":
        return jsonify({"error": "No se proporcionó un archivo en el formulario."}, 400)

    # Obtener los parámetros
    id_imagen = request.args.get("id_imagen", "")
    peso = request.args.get("peso", "")
    altura = request.args.get("altura", "")
    fecha_nacimiento = request.args.get("fecha_nacimiento", "")
    sexo = request.args.get("sexo", "")
    inestabilidad = request.args.get("inestabilidad", "")
    cajon_anterior_positivo = request.args.get("cajon_anterior_positivo", "")
    impotencia_funcional = request.args.get("impotencia_funcional", "")

    # Trabajo con el archivo zip
    with tempfile.TemporaryDirectory() as temp_dir:
        zip_data = request.files["file"].read()
        with ZipFile(BytesIO(zip_data), "r") as zip_ref:
            zip_ref.extractall(temp_dir)
            file_list = os.listdir(temp_dir)

            if len(file_list) != 9:
                return jsonify(
                    {
                        "error": "Debe contener 9 imágenes, que representan una rodilla vista en 3D"
                    },
                    400,
                )

            # Verificar que los archivos sean imágenes
            for file_name in file_list:
                if not file_name.lower().endswith((".jpg", ".jpeg", ".png", ".gif")):
                    return jsonify(
                        {
                            "error": "El archivo ZIP contiene archivos que no son imágenes válidas."
                        },
                        400,
                    )

            # Obtener predicciones
            prediction = predict_lca(temp_dir)

            # Obtener la imagen del medio
            file_list.sort()
            middle_index = len(file_list) // 2
            middle_image_path = os.path.join(temp_dir, file_list[middle_index])
            image_path = os.path.join(temp_dir, middle_image_path)

            # Convertir la imagen a base64 para enviarla después
            with open(middle_image_path, "rb") as middle_image_file:
                middle_image_data = middle_image_file.read()
                base64_image = base64.b64encode(middle_image_data).decode("utf-8")

            # Ruta de la nueva imagen con el id
            new_image_name = (
                f"{id_imagen}.jpg"  # Cambia la extensión si es diferente de .jpg
            )
            new_image_path = os.path.join(temp_dir, new_image_name)

            # Renombra la imagen
            os.rename(image_path, new_image_path)

            imagen = "images/" + new_image_name

            # Ahora puedes subir la imagen renombrada
            upload_file(imagen, new_image_path)

            csv_data = [
                [
                    id_imagen,
                    imagen,
                    fecha_nacimiento,
                    peso,
                    altura,
                    sexo,
                    inestabilidad,
                    cajon_anterior_positivo,
                    impotencia_funcional,
                    "",
                    "",
                ]
            ]

            if not exist_bd("BaseDeDatosLCA.csv"):
                saveInfo_csv_file(
                    temp_dir + "/BaseDeDatosLCA.csv", csv_headers, csv_data
                )
                upload_file("bd/BaseDeDatosLCA.csv", temp_dir + "/BaseDeDatosLCA.csv")
            else:
                csv_lca = temp_dir + "/BaseDeDatosLCA.csv"
                download_file("bd/BaseDeDatosLCA.csv", csv_lca)
                saveInfo_csv_file(
                    temp_dir + "/BaseDeDatosLCA.csv", csv_headers, csv_data
                )
                upload_file("bd/BaseDeDatosLCA.csv", temp_dir + "/BaseDeDatosLCA.csv")

    return jsonify({"image": base64_image, "prediction": prediction})


@app.route("/feedback-lca", methods=["POST"])
def feedback_lca_endpoint():
    id_imagen = request.args.get("id_imagen", "")
    rotura_lca = bool(request.args.get("rotura_lca", "false") != "false")
    sano = bool(request.args.get("sano", "false") != "false")
    comentario = request.args.get("comentario", "")

    campos_booleanos = [
        rotura_lca,
        sano,
    ]

    # Verifica que exactamente un campo sea True
    if campos_booleanos.count(True) > 1:
        return (
            jsonify({"error": "Exactamente un campo debe ser verdadero (True)."}),
            400,
        )

    respuesta_resultado = None

    if rotura_lca:
        respuesta_resultado = "Rotura LCA"
    elif sano:
        respuesta_resultado = "LCA sano"

    with tempfile.TemporaryDirectory() as temp_dir:
        csv_lca = temp_dir + "/BaseDeDatosLCA.csv"
        download_file("bd/BaseDeDatosLCA.csv", csv_lca)
        updateInfo_csv_file(
            csv_lca, id_imagen, respuesta_resultado, comentario, csv_headers
        )
        upload_file("bd/BaseDeDatosLCA.csv", csv_lca)

    return "Feedback guardado correctamente."


################## EGC ##################


@app.route("/predict-egc", methods=["POST"])
def predict_egc_endpoint():
    if "file" not in request.files:
        return jsonify(
            {"error": "No se proporcionó una imagen válida en el formulario."}, 400
        )

    imagen = request.files["file"]
    id_imagen = request.args.get("id_imagen", "")
    peso = request.args.get("peso", "")
    altura = request.args.get("altura", "")
    fecha_nacimiento = request.args.get("fecha_nacimiento", "")
    sexo = request.args.get("sexo", "")
    palpitaciones = request.args.get("palpitaciones", "")
    dolor = request.args.get("dolor_superior_izquierdo", "")
    disnea = request.args.get("disnea", "")

    with tempfile.TemporaryDirectory() as temp_dir:
        image_filename = f"{id_imagen}.jpg"
        image_path = os.path.join(temp_dir, image_filename)
        imagen.save(image_path)
        imagen = "images/" + image_filename
        upload_file(imagen, image_path)

        csv_data = [
            [
                id_imagen,
                imagen,
                fecha_nacimiento,
                peso,
                altura,
                sexo,
                dolor,
                palpitaciones,
                disnea,
                "",
                "",
            ]
        ]

        if not exist_bd("BaseDeDatosEGC.csv"):
            saveInfo_csv_file(temp_dir + "/BaseDeDatosEGC.csv", csv_headers, csv_data)
            upload_file("bd/BaseDeDatosEGC.csv", temp_dir + "/BaseDeDatosEGC.csv")
        else:
            csv_egc = temp_dir + "/BaseDeDatosEGC.csv"
            download_file("bd/BaseDeDatosEGC.csv", csv_egc)
            saveInfo_csv_file(temp_dir + "/BaseDeDatosEGC.csv", csv_headers, csv_data)
            upload_file("bd/BaseDeDatosEGC.csv", temp_dir + "/BaseDeDatosEGC.csv")

        results = predict_egc(image_path)

    return jsonify(results)


@app.route("/feedback-egc", methods=["POST"])
def feedback_egc_endpoint():
    id_imagen = request.args.get("id_imagen", "")
    fusion = bool(request.args.get("fusion", "false") != "false")
    infarto = bool(request.args.get("infarto", "false") != "false")
    latido_normal = bool(request.args.get("latido_normal", "false") != "false")
    no_clasificable = bool(request.args.get("no_clasificable", "false") != "false")
    prematuro_supraventricular = bool(
        request.args.get("prematuro_supraventricular", "false") != "false"
    )
    contraccion = bool(request.args.get("contraccion", "false") != "false")
    comentario = request.args.get("comentario", "")

    campos_booleanos = [
        fusion,
        infarto,
        latido_normal,
        no_clasificable,
        prematuro_supraventricular,
        contraccion,
    ]

    # Verifica que exactamente un campo sea True
    if campos_booleanos.count(True) > 1:
        return (
            jsonify({"error": "Exactamente un campo debe ser verdadero (True)."}),
            400,
        )

    respuesta_resultado = None

    if fusion:
        respuesta_resultado = "Fusion de latido ventricular y normal"
    elif infarto:
        respuesta_resultado = "Infarto de miocardio"
    elif latido_normal:
        respuesta_resultado = "Latido normal"
    elif no_clasificable:
        respuesta_resultado = "Latido no clasificable"
    elif prematuro_supraventricular:
        respuesta_resultado = "Latido prematuro supraventricular"
    elif contraccion:
        respuesta_resultado = "Contraccion ventricular prematura"

    with tempfile.TemporaryDirectory() as temp_dir:
        csv_egc = temp_dir + "/BaseDeDatosEGC.csv"
        download_file("bd/BaseDeDatosEGC.csv", csv_egc)
        updateInfo_csv_file(
            csv_egc, id_imagen, respuesta_resultado, comentario, csv_headers
        )
        upload_file("bd/BaseDeDatosEGC.csv", csv_egc)

    return "Feedback guardado correctamente."


################## Fractura ##################


@app.route("/predict-fracture", methods=["POST"])
def predict_fracture_endpoint():
    if "file" not in request.files:
        return jsonify(
            {"error": "No se proporcionó una imagen válida en el formulario."}, 400
        )

    imagen = request.files["file"]
    id_imagen = request.args.get("id_imagen", "")
    peso = request.args.get("peso", "")
    altura = request.args.get("altura", "")
    fecha_nacimiento = request.args.get("fecha_nacimiento", "")
    sexo = request.args.get("sexo", "")
    dolor_con_limitacion = request.args.get("dolor_con_limitacion", "")
    edema = request.args.get("edema", "")
    deformidad = request.args.get("deformidad", "")

    with tempfile.TemporaryDirectory() as temp_dir:
        image_filename = f"{id_imagen}.jpg"
        image_path = os.path.join(temp_dir, image_filename)
        imagen.save(image_path)
        imagen = "images/" + image_filename
        upload_file(imagen, image_path)

        csv_data = [
            [
                id_imagen,
                imagen,
                fecha_nacimiento,
                peso,
                altura,
                sexo,
                dolor_con_limitacion,
                edema,
                deformidad,
                "",
                "",
            ]
        ]

        if not exist_bd("BaseDeDatosFrac.csv"):
            saveInfo_csv_file(temp_dir + "/BaseDeDatosFrac.csv", csv_headers, csv_data)
            upload_file("bd/BaseDeDatosFrac.csv", temp_dir + "/BaseDeDatosFrac.csv")
        else:
            csv_frac = temp_dir + "/BaseDeDatosFrac.csv"
            download_file("bd/BaseDeDatosFrac.csv", csv_frac)
            saveInfo_csv_file(temp_dir + "/BaseDeDatosFrac.csv", csv_headers, csv_data)
            upload_file("bd/BaseDeDatosFrac.csv", temp_dir + "/BaseDeDatosFrac.csv")

        results = predict_fracture(image_path)

    return jsonify(results)


@app.route("/feedback-fracture", methods=["POST"])
def feedback_fracture_endpoint():
    id_imagen = request.args.get("id_imagen", "")
    fractura = bool(request.args.get("fractura", "false") != "false")
    sano = bool(request.args.get("sano", "false") != "false")
    comentario = request.args.get("comentario", "")

    campos_booleanos = [
        fractura,
        sano,
    ]

    # Verifica que exactamente un campo sea True
    if campos_booleanos.count(True) > 1:
        return (
            jsonify({"error": "Exactamente un campo debe ser verdadero (True)."}),
            400,
        )

    respuesta_resultado = None

    if fractura:
        respuesta_resultado = "Fractura"
    elif sano:
        respuesta_resultado = "Sano"

    with tempfile.TemporaryDirectory() as temp_dir:
        csv_egc = temp_dir + "/BaseDeDatosFrac.csv"
        download_file("bd/BaseDeDatosFrac.csv", csv_egc)
        updateInfo_csv_file(
            csv_egc, id_imagen, respuesta_resultado, comentario, csv_headers
        )
        upload_file("bd/BaseDeDatosFrac.csv", csv_egc)

    return "Feedback guardado correctamente."


@app.route("/ping", methods=["GET"])
def ping_endpoint():
    return jsonify({"message": "API is up and running"})


@app.route("/download-bd-egc", methods=["POST"])
def descargar_bd_egc():
    texto = request.form.get("texto", "")

    if texto != "":
        sha256 = hashlib.sha256()
        sha256.update(texto.encode("utf-8"))
        hash_texto = sha256.hexdigest()

        if hash_texto in Valid_hash.valid_hash:
            csv_egc = "./src/BaseDeDatosEGC.csv"
            download_file("bd/BaseDeDatosEGC.csv", csv_egc)

            return send_file(csv_egc, as_attachment=True)

        print("No tienes permiso para descargar la base de datos.")
        return (
            jsonify({"error": "No tienes permiso para descargar la base de datos."}),
            400,
        )

    return jsonify({"error": "No se proporcionó ningún texto en la solicitud."}), 400

@app.route("/download-bd-fracture", methods=["POST"])
def descargar_bd_frac():
    texto = request.form.get("texto", "")

    if texto != "":
        sha256 = hashlib.sha256()
        sha256.update(texto.encode("utf-8"))
        hash_texto = sha256.hexdigest()

        if hash_texto in Valid_hash.valid_hash:
            csv_frac = "./src/BaseDeDatosFrac.csv"
            download_file("bd/BaseDeDatosFrac.csv", csv_frac)

            return send_file(csv_frac, as_attachment=True)

        return (
            jsonify({"error": "No tienes permiso para descargar la base de datos."}),
            400,
        )

    return jsonify({"error": "No se proporcionó ningún texto en la solicitud."}), 400

@app.route("/download-bd-lca", methods=["POST"])
def descargar_bd_lca():
    texto = request.form.get("texto", "")

    if texto != "":
        sha256 = hashlib.sha256()
        sha256.update(texto.encode("utf-8"))
        hash_texto = sha256.hexdigest()

        if hash_texto in Valid_hash.valid_hash:
            csv_frac = "./src/BaseDeDatosLCA.csv"
            download_file("bd/BaseDeDatosLCA.csv", csv_frac)

            return send_file(csv_frac, as_attachment=True)

        return (
            jsonify({"error": "No tienes permiso para descargar la base de datos."}),
            400,
        )

    return jsonify({"error": "No se proporcionó ningún texto en la solicitud."}), 400

if __name__ == "__main__":
    app.run()
