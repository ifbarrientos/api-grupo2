from google.cloud import storage
import os

# Credenciales de acceso
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./static/magnetic-port-402718-061f817bf01f.json"
project_id = 'magnetic-port-402718'
bucket_name = 'feedback-api'

# Inicializa el cliente de almacenamiento
storage_client = storage.Client(project=project_id)

# Selecciona el bucket
bucket = storage_client.get_bucket(bucket_name)

# Sube el archivo
def upload_file(destination_blob_name, source_file_name):
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)

def download_file(source_blob_name, destination_file_name):
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)

def exist_bd(nombre_archivo):
    blob = bucket.blob("bd/" + nombre_archivo)
    return blob.exists()