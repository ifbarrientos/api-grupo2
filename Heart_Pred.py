import numpy as np
import tensorflow as tf
from tensorflow.keras.models import load_model
import cv2

# Cargar el modelo entrenado
model = load_model("./src/models/Heart_model.h5")


# Función para preprocesar la imagen en escala de grises
def preprocess_image(image):
    target_size = (224, 224)
    if image.shape[2] == 3:  # Si la imagen es RGB
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.resize(image, target_size)
    image = image / 255.0
    return np.expand_dims(
        image, axis=-1
    )  # Añadir una dimensión para representar los canales


# Función para preparar generadores de imágenes en escala de grises
def prepare_image_generator(dataframe, subset):
    generator = tf.keras.preprocessing.image.ImageDataGenerator(
        preprocessing_function=preprocess_image,
    )

    return generator.flow_from_dataframe(
        dataframe=dataframe,
        x_col="Filepath",
        y_col="Label",
        target_size=(224, 224),
        color_mode="grayscale",
        class_mode="categorical",
        shuffle=True if subset == "training" else False,
        seed=42,
        subset=subset,
    )


# Función para cargar imágenes y etiquetas
def predict_egc(image_path):
    # Cargar la imagen y preprocesarla
    image = cv2.imread(image_path)
    preprocessed_image = preprocess_image(image)
    preprocessed_image = np.expand_dims(
        preprocessed_image, axis=0
    )  # Agregar dimensión del lote

    # Realizar la predicción
    predictions = model.predict(preprocessed_image)

    # Obtener la etiqueta predicha
    labels = [
        "fusionVentricularNormal",
        "infarto",
        "normal",
        "no_clasificable",
        "prematuroSupraventricular",
        "contraccionVentricular",
    ]

    results = {}

    for label, prediction in zip(labels, predictions.flatten()):
        results[label] = round(float(prediction), 2)

    return results
