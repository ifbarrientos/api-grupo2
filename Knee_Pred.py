import os
import cv2
import numpy as np
from keras.models import load_model

# Cargar modelo
model = load_model('./src/models/Knee_model.h5')

def predict_lca(ruta_output):
    imagenes = []

    archivos = os.listdir(ruta_output)

    # Leer cada imagen, convertirla a escala de grises y normalizar
    for i in archivos:
        imagen_path = os.path.join(ruta_output, f"{i}")
        imagen = preprocess_image(cv2.imread(imagen_path), (128, 128))
        imagenes.append(imagen)

        # Convertir a formato numpy y agregar dimensión del canal
    imagenes_np = np.array(imagenes)
    imagenes_np = np.expand_dims(imagenes_np, axis=-1)

        # Realizar la predicción
    predicciones = model.predict(np.array([imagenes_np]))
    predicciones = predicciones[0].flatten()

    # La salida será un valor entre 0 y 1
    result = {
        "roturaLCA": round(float(predicciones)*100, 2),
        "lcaSano": round((1 - float(predicciones))*100, 2),
    }
    return result

def preprocess_image(image, image_size):
    # Convierte a escala de grises
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Normaliza y redimensiona la imagen
    img_resized = cv2.resize(img_gray, image_size)
    img_resized = img_resized / 255.0
    return img_resized