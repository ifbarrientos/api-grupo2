import cv2
import numpy as np
from keras.models import load_model

# Cargar el modelo preentrenado
model = load_model("./src/models/Wrist_model.h5")

image_size = (256, 256)

# Función para cargar y preprocesar imágenes de la carpeta "valid"
def predict_fracture(image_path):

    image = cv2.imread(image_path)
    preprocessed_image = preprocess_image(image, image_size)
    preprocessed_image = np.expand_dims(preprocessed_image, axis=0)

    predictions = model.predict(preprocessed_image)
    predictions = predictions[0].flatten()

    result = {
        "fractura": round(float(predictions)*100, 2),
        "sano": round((1 - float(predictions))*100, 2),
    }

    return result

def preprocess_image(image, image_size):
    img_resized = cv2.resize(image, image_size)
    img_resized = img_resized / 255.0
    return img_resized
