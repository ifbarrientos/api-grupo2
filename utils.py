import os
import csv
from Upload_GS import *

def saveInfo_csv_file(csv_file, csv_headers, csv_data):
    if not os.path.isfile(csv_file):
        with open(csv_file, "w", newline="") as file:
            writer = csv.writer(file)
            writer.writerow(csv_headers)

    # Abre el archivo en modo escritura
    with open(csv_file, "a", newline="") as file:
        writer = csv.writer(file)

        # Escribe los datos en el archivo CSV
        writer.writerows(csv_data)


def updateInfo_csv_file(
    csv_file, id_imagen, respuesta_resultado, comentario, csv_headers
):
    rows = []

    with open(csv_file, "r", newline="") as file:
        reader = csv.DictReader(file)
        for row in reader:
            rows.append(row)

    # Actualizar la fila correspondiente al id_imagen
    for row in rows:
        if row["ID_Imagen"] == id_imagen:
            row["Respuesta_Medico"] = respuesta_resultado
            row["Comentario"] = comentario

    # Escribir la información actualizada en el archivo CSV
    with open(csv_file, "w", newline="") as file:
        writer = csv.DictWriter(file, fieldnames=csv_headers)
        writer.writeheader()
        writer.writerows(rows)


def  saveCsvData(temp_dir, database, csv_headers, csv_data):
    if not exist_bd(database):
                saveInfo_csv_file(temp_dir + "/{database}", csv_headers, csv_data)
                upload_file("bd/{database}", temp_dir + "/{database}")
    else:
                csv_lca = temp_dir + "/{database}"
                download_file("bd/{database}", csv_lca)
                saveInfo_csv_file(temp_dir + "/{database}", csv_headers, csv_data)
                upload_file("bd/{database}", temp_dir + "/{database}")
